## Scrumptious: Shopping Lists

1. Should be in the **recipes** app
2. Create `ShoppingItem` model in ***recipes\models.py***
    - Fields
        - `user = models.ForeignKey(USER_MODEL, related_name="shopping_item", on_delete=models.CASCADE,)`
        - `food_item = models.ForeignKey("FoodItem", on_delete=models.PROTECT)`
        - The combination of those two values should be **unique**
            - Check `unique_together` or `UniqueConstraint`
3. Views
    - class `CartListView`
        - Shopping items created by the user (get_query filter)
        - `template_name = "recipes/shopping_items.html"`
    - Function view to **add** an ingredient's food item to the shopping list
        - **only** HTTP POST requests
        - creates a `ShoppingItem` instance based on current user and the value of the submitted "ingredients_id"
    - Function view and button for **deleting** all of the shopping items
        - **only** HTTP POST requests
        - Should delete `ShoppingItem` instances associated with the current user
        - Should show empty shopping list after the deletion
            - redirect to `recipes/shopping_items/`
    - **Import views into ***recipes\urls.py***
4. Main NAV in ***base.html*** to include an anchor href to ***shopping_list.html***
    - Just name it "Shopping List"
    - Nav should include a count of how many items are in the shopping list for that user

### Not sure which step:
- A "+ shopping list" button next to any ingredient not already in the shopping list that, when clicked, adds it to the shopping list
- A button on the list page to clear all items from the shopping list
