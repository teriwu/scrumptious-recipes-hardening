## Requirements Road Map

1. Create Django app namned `meal_plans`
2. Create a MealPlan model
    <br>Add fields to model</br>
    1. Name
    2. Date
    3. Owner (ForeignKey)
    4. Recipe
3. Create our 5 paths
4. Create `ListView`
    1. Should only show meal pleans created by the user
    2. Add link to the `CreateView`
    3. Each meal plan should be a link to its `DetailView`
5. Create `CreateView`
    1. This view will show a form othat allows the user to enter a name, a date, and seelc the recipes they want to put in this meal plan.
    2. When the user saves the meal plan, their user object should be automatically saved to the owner property of the meal plan.
    3. Then the user should be redirected to the detail page for the newly-created meal plan.
    4. There is also an exrta piece of code that you have to include in the `CreateView` to make sure the recipes are saved with the meal plan.
6. Create `DetailView`
    1. This view will show the name of the meal plan, the date that it is to be served, and the list of the names of the recipes associated with it.
    2. Each recipe name should be a link to the recipe page.
    3. This page should have a link to the `EditView`,
    4. we need a link to the `DeleteView`.
    5. If the person accessing this page is not the owner of the meal plan, then they should not see the meal plan.
7. Create our `EditView`
    1. Show a form that allows the person who owns the meal plan to change it.
    2. If the person accessing this page is not the owner of the meal plan, then they should not see the meal plan.
8. Create the `DeleteView`
    1. Show a form that allows the person who owns the meal plan to delete it.
    2. After successful deletion, the user should be redirected back to the list view for meal plans.
    3. If the person accessing this page is not the owner of the meal plan, then they should not see the meal plan.


### Templates
- All of the Django HTML tempaltes that you write should extend the base.html file that you created in a previous step.
- Alter the ***base.html*** to show a "Login" link in the navigation list when the current user is not authenticated. The Dhjango HTML for that should look liek this. You'll have to figure out where it goes, of course.
