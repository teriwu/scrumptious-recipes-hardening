# Generated by Django 4.0.3 on 2022-06-09 19:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meal_plans', '0003_alter_mealplan_recipes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mealplan',
            name='date',
            field=models.DateField(auto_now=True),
        ),
    ]
